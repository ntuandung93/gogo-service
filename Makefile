IMG_NAME := joeng93/gogo-service
IMG_TAG := $$(git rev-parse --short=8 HEAD)
IMG_TAG_CI := ${CI_COMMIT_SHORT_SHA}

# Use @.. to avoid outputting the command itself

test:
	@go test -v ./...

build-bin:
	@go build -o gogo-service
build-img:
	@docker build -t ${IMG_NAME}:${IMG_TAG} .
	@docker tag ${IMG_NAME}:${IMG_TAG} ${IMG_NAME}:latest
build-img-ci:
	# One-liner for 2 commands in build-img
	@docker build -t ${IMG_NAME}:latest -t ${IMG_NAME}:${IMG_TAG_CI} .

push-img:
	@docker push ${IMG_NAME}

login:
	@docker login -u ${DOCKER_USER} -p ${DOCKER_PASS}