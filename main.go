package main

import (
	"gogo-service/service"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	server := service.NewServer()
	server.Run(":" + port)
}
