FROM golang:alpine AS build-env

RUN apk add --no-cache git

WORKDIR /build

# Cache dependencies
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

RUN go build -o gogo-service

# ----------------------------------------

FROM alpine

WORKDIR /app

COPY --from=build-env /build/gogo-service .

ENTRYPOINT [ "./gogo-service" ]
