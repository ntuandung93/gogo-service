package service

import "time"

import uuid "github.com/satori/go.uuid"

type gameBoard struct {
	Positions [][]byte
}

func newBoard(size int) gameBoard {
	outBoard := gameBoard{}
	a := make([][]byte, size)
	for i := range a {
		a[i] = make([]byte, size)
	}
	outBoard.Positions = a
	return outBoard
}

type match struct {
	TurnCount   int
	GridSize    int
	ID          string
	StartTime   time.Time
	GameBoard   gameBoard
	PlayerBlack string
	PlayerWhite string
}

func newMatch(size int, playerBlackName, playerWhiteName string) match {
	result := match{}
	result.ID = uuid.NewV4().String()
	result.StartTime = time.Now()
	result.GameBoard = newBoard(size)
	result.TurnCount = 0
	result.GridSize = size
	result.PlayerBlack = playerBlackName
	result.PlayerWhite = playerWhiteName

	return result
}

type newMatchResponse struct {
	ID          string `json:"id"`
	StartedAt   int64  `json:"started_at"`
	GridSize    int    `json:"grid_size"`
	PlayerWhite string `json:"player_white"`
	PlayerBlack string `json:"player_back"`
	Turn        int    `json:"turn,omitempty"`
}

func (m *newMatchResponse) copyMatch(match match) {
	m.ID = match.ID
	m.StartedAt = match.StartTime.Unix()
	m.GridSize = match.GridSize
	m.PlayerWhite = match.PlayerWhite
	m.PlayerBlack = match.PlayerBlack
	m.Turn = match.TurnCount
}

type newMatchRequest struct {
	GridSize    int    `json:"grid_size"`
	PlayerWhite string `json:"player_white"`
	PlayerBlack string `json:"player_black"`
}

func (request newMatchRequest) isValid() bool {
	if request.GridSize != 19 && request.GridSize != 13 && request.GridSize != 9 {
		return false
	}
	if request.PlayerWhite == "" {
		return false
	}
	if request.PlayerBlack == "" {
		return false
	}
	return true
}
