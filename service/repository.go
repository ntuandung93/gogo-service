package service

type matchRepository interface {
	addMatch(m match) error
	getMatches() ([]match, error)
}
