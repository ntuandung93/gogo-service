package service

type inMemRepository struct {
	matches []match
}

func newInmemRepository() *inMemRepository {
	repo := &inMemRepository{}
	repo.matches = []match{}
	return repo
}

func (repo *inMemRepository) addMatch(m match) error {
	repo.matches = append(repo.matches, m)
	return nil
}

func (repo *inMemRepository) getMatches() ([]match, error) {
	return repo.matches, nil
}
