module gogo-service

go 1.12

require (
	github.com/codegangsta/negroni v1.0.0
	github.com/gorilla/mux v1.7.3
	github.com/satori/go.uuid v1.2.0
	github.com/unrolled/render v1.0.1
)
